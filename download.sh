#!/bin/bash

DESTINATION=$1

# clone Plausible directory
git clone --depth=1 https://gitlab.com/6ministers/business-apps/plausible-open-source-web-analytics-docker-compose $DESTINATION
rm -rf $DESTINATION/.git

