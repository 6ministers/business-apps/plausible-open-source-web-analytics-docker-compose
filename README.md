# Installing Plausible open source web analytics docker compose.

## Quick Installation

**Before starting the instance, direct the domain (subdomain) to the ip of the server where Plausible will be installed!**

## 1. Plausible Requirements
From and more
- 2 CPUs
- 2 RAM 
- 10 Gb 

Run for Ubuntu 22.04

``` bash
sudo apt-get purge needrestart
```

## 2.Install docker and docker-compose:

``` bash
curl -s https://gitlab.com/6ministers/business-apps/plausible-open-source-web-analytics-docker-compose/-/raw/master/setup.sh | sudo bash -s
```

## 3.Download Plausible instance:


``` bash
curl -s https://gitlab.com/6ministers/business-apps/plausible-open-source-web-analytics-docker-compose/-/raw/master/download.sh | sudo bash -s plausible
```

If `curl` is not found, install it:

``` bash
$ sudo apt-get install curl
# or
$ sudo yum install curl
```

Go to the catalog

``` bash
cd plausible
```


In the downloaded directory you'll find two important files:

docker-compose.yml - installs and orchestrates networking between your Plausible CE server, Postgres database, Clickhouse database (for stats), and an SMTP server.
plausible-conf.env - configures the Plausible server itself. Full configuration options are documented below.

Right now the latter looks like this:

`plausible-conf.env`
``` bash
BASE_URL=replace-me
SECRET_KEY_BASE=replace-me
```
Let's do as it asks and populate these required environment variables with our own values.

First we generate the SECRET_KEY_BASE using openssl

console
``` bash
$ openssl rand -base64 48
GLVzDZW04FzuS1gMcmBRVhwgd4Gu9YmSl/k/TqfTUXti7FLBd7aflXeQDdwCj6Cz
```
And then we decide on the BASE_URL where the instance would be accessible. Let's assume we choose http://plausible.example.com

`plausible-conf.env`

``` bash
- BASE_URL=replace-me
+ BASE_URL=http://plausible.example.com
- SECRET_KEY_BASE=replace-me
+ SECRET_KEY_BASE=GLVzDZW04FzuS1gMcmBRVhwgd4Gu9YmSl/k/TqfTUXti7FLBd7aflXeQDdwCj6Cz
```
We can start our instance now but the requests would be served over HTTP. Not cool! Let's configure Caddy to enable HTTPS.


To change the domain in the `Caddyfile` to your own

``` bash
https://plausible.your-domain.com:443 {
    reverse_proxy 127.0.0.1:8000
#	tls admin@example.org
	encode zstd gzip
    file_server

...	
}
```


## 5.Run Plausible:

``` bash
docker-compose up -d
```

Then open `https://plausible.domain.com:` to access OpenLDAP+PhpLdapAdmin



## Plausible container management

**Run**:

``` bash
docker-compose up -d
```

**Restart**:

``` bash
docker-compose restart
```

**Restart**:

``` bash
sudo docker-compose down && sudo docker-compose up -d
```

**Stop**:

``` bash
docker-compose down
```

## Documentation

https://github.com/plausible/community-edition/tree/v2.0.0

https://www.odoo.com/documentation/master/applications/websites/website/reporting/analytics.html


